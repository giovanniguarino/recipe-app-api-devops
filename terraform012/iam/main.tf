provider "aws" {
    region = "us-east-1"
}

resource "aws_iam_user" "myUser" {
    name = "testuser"
  
}

resource "aws_iam_policy" "customPolicy" {
    name = "GlacierEFSEC2"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "ec2:DeleteManagedPrefixList",
                "ec2:AuthorizeSecurityGroupIngress",
                "ec2:ReplaceRouteTableAssociation",
                "ec2:ModifyManagedPrefixList",
                "ec2:DeleteVpcEndpoints",
                "ec2:ResetInstanceAttribute",
                "ec2:CreateIpamPool",
                "ec2:RejectTransitGatewayMulticastDomainAssociations",
                "ec2:ResetEbsDefaultKmsKeyId",
                "ec2:AttachInternetGateway",
                "ec2:ReportInstanceStatus",
                "ec2:ModifyVpnConnectionOptions",
                "ec2:ReleaseIpamPoolAllocation",
                "ec2:DeleteVpnGateway",
                "ec2:CreateRoute",
                "elasticfilesystem:ClientMount",
                "ec2:CreateCoipPoolPermission",
                "ec2:DeleteNetworkInsightsAnalysis",
                "ec2:UnassignPrivateIpAddresses",
                "glacier:PurchaseProvisionedCapacity",
                "glacier:ListParts",
                "ec2:UpdateSecurityGroupRuleDescriptionsIngress",
                "ec2:DeleteRouteTable",
                "ec2:CreateNetworkInsightsPath",
                "ec2:ModifySpotFleetRequest",
                "ec2:AllocateIpamPoolCidr",
                "ec2:StartInstances",
                "ec2:RevokeSecurityGroupEgress",
                "ec2:CreateInternetGateway",
                "ec2:RestoreSnapshotTier",
                "ec2:DeleteInternetGateway",
                "ec2:RejectTransitGatewayVpcAttachment",
                "ec2:CreateReservedInstancesListing",
                "ec2:DisassociateTransitGatewayRouteTable",
                "ec2:ModifyAddressAttribute",
                "ec2:BundleInstance",
                "ec2:DisableFastSnapshotRestores",
                "ec2:DeregisterTransitGatewayMulticastGroupMembers",
                "ec2:CreateTags",
                "glacier:DeleteVault",
                "ec2:ModifyNetworkInterfaceAttribute",
                "glacier:DeleteArchive",
                "ec2:CreateLocalGatewayRouteTableVpcAssociation",
                "ec2:RunInstances",
                "ec2:ModifySecurityGroupRules",
                "ec2:AssignPrivateIpAddresses",
                "ec2:DisassociateRouteTable",
                "ec2:RevokeSecurityGroupIngress",
                "ec2:DetachVpnGateway",
                "ec2:CreateDefaultVpc",
                "ec2:DisassociateIamInstanceProfile",
                "glacier:ListMultipartUploads",
                "ec2:CreateSnapshots",
                "ec2:DeleteIpamPool",
                "ec2:CreateSubnet",
                "ec2:CreateLocalGatewayRouteTablePermission",
                "ec2:DeleteNetworkAclEntry",
                "ec2:AttachVolume",
                "ec2:ExportImage",
                "ec2:DisassociateAddress",
                "ec2:ExportTransitGatewayRoutes",
                "ec2:AssociateInstanceEventWindow",
                "ec2:CreateNatGateway",
                "ec2:AcceptTransitGatewayPeeringAttachment",
                "glacier:CreateVault",
                "ec2:ModifyTrafficMirrorFilterNetworkServices",
                "ec2:ModifyVpnConnection",
                "ec2:StartNetworkInsightsAnalysis",
                "ec2:ModifyIpamScope",
                "ec2:ModifyImageAttribute",
                "ec2:DisableIpamOrganizationAdminAccount",
                "glacier:DeleteVaultNotifications",
                "ec2:EnableFastSnapshotRestores",
                "ec2:RejectTransitGatewayPeeringAttachment",
                "ec2:CreateDefaultSubnet",
                "ec2:CreateCapacityReservation",
                "ec2:CreateSpotDatafeedSubscription",
                "ec2:ModifyVpnTunnelCertificate",
                "ec2:ModifyInstanceEventWindow",
                "ec2:RebootInstances",
                "ec2:CreateInstanceExportTask",
                "ec2:DeleteTrafficMirrorFilter",
                "ec2:DeleteLaunchTemplate",
                "ec2:CreateRestoreImageTask",
                "ec2:AssignIpv6Addresses",
                "ec2:ImportInstance",
                "ec2:AssociateEnclaveCertificateIamRole",
                "ec2:AcceptVpcEndpointConnections",
                "ec2:ModifyFpgaImageAttribute",
                "ec2:CancelConversionTask",
                "ec2:ImportSnapshot",
                "ec2:DeprovisionPublicIpv4PoolCidr",
                "ec2:RevokeClientVpnIngress",
                "ec2:DeleteTrafficMirrorSession",
                "ec2:DisassociateSubnetCidrBlock",
                "ec2:RegisterTransitGatewayMulticastGroupMembers",
                "ec2:AuthorizeClientVpnIngress",
                "ec2:CreateLaunchTemplate",
                "ec2:ImportClientVpnClientCertificateRevocationList",
                "ec2:DisableVpcClassicLink",
                "ec2:DisableVpcClassicLinkDnsSupport",
                "ec2:AllocateHosts",
                "ec2:CancelImportTask",
                "ec2:DeleteInstanceEventWindow",
                "ec2:ModifyIdFormat",
                "ec2:DeleteFlowLogs",
                "ec2:CopySnapshot",
                "ec2:ModifyVpcEndpointServiceConfiguration",
                "ec2:UnmonitorInstances",
                "ec2:DeleteNetworkInsightsAccessScope",
                "ec2:RegisterInstanceEventNotificationAttributes",
                "elasticfilesystem:DescribeReplicationConfigurations",
                "glacier:AbortMultipartUpload",
                "ec2:DetachClassicLinkVpc",
                "glacier:ListProvisionedCapacity",
                "ec2:ResetFpgaImageAttribute"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_policy_attachment" "policyBind" {
    name = "attachment"
    users = [aws_iam_user.myUser.name]
    policy_arn = aws_iam_policy.customPolicy.arn
}