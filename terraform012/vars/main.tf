provider "aws" {
    region = "us-east-1"
}

variable "nos" {
  type = number
}

resource "aws_instance" "ec2" {
  ami = "ami-0c293f3f676ec4f90"
  instance_type = "t2.micro"
  count = var.nos
}