provider "aws" {
    region = "us-east-1"
}

module "web_server_instance" {
    source = "./ec2"
    ec2name = "Web Server"
    security_groups = [module.web_server_security_groups.group_name]
    user_data = file("server-script.sh")
}

module "db_server_instance" {
    source = "./ec2"
    ec2name = "DB Server"
}

module "web_server_security_groups" {
    source = "./sg"
    name = "Allow Web Traffic"
    ingressrules = [80, 443]
    egressrules = [80, 443]
    
}

module "web_server_ip" {
    source = "./eip"
    instance_id = module.web_server_instance.instance_id
}

output "db_server_private_id" {
  value = module.db_server_instance.private_ip
}

output "web_server_public_ip" {
  value = module.web_server_instance.public_ip
}