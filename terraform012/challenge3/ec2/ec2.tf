variable "ec2name" {
  type = string
}

variable "security_groups" {
    type = list(string)
    default = []
}

variable "user_data" {
  type = string
  default = ""
}

resource "aws_instance" "ec2" {
  ami = "ami-0c293f3f676ec4f90"
  instance_type = "t2.micro"
  security_groups = var.security_groups
  user_data = var.user_data
  tags = {
    Name = var.ec2name
  }

}

output "instance_id" {
  value = aws_instance.ec2.id
}

output "private_ip" {
  value = aws_instance.ec2.private_ip
}

output "public_ip" {
  value = aws_instance.ec2.public_ip
}