variable "ingressrules" {
  type = list(number)
}

variable "egressrules" {
  type = list(number)
}

variable "name" {
  type = string
}

resource "aws_security_group" "sg" {
    name = var.name
    
    dynamic ingress{
      iterator  = port
      for_each = var.ingressrules
      content{
      from_port = port.value
      to_port = port.value
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]  
      }
    }

    dynamic egress{
      iterator  = port
      for_each = var.egressrules
      content{
      from_port = port.value
      to_port = port.value
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]  
      }
    }
}

output "group_name" {
  value = aws_security_group.sg.name
}