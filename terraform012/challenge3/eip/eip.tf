variable "instance_id" {
  type = string
}

resource "aws_eip" "elasticip" {
  instance = var.instance_id
}