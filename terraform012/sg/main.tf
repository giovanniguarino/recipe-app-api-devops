provider "aws" {
    region = "us-east-1"
}

resource "aws_instance" "ec2" {
  ami = "ami-0c293f3f676ec4f90"
  instance_type = "t2.micro"
  security_groups = [aws_security_group.webtraffic.name]
}

resource "aws_security_group" "webtraffic"{
  name = "Allow HTTPS"
  

  ingress = [{      
    description      = "HTTPS"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]  
    ipv6_cidr_blocks = []
    prefix_list_ids = []
    security_groups = []
    self = false
    }]

  egress = [{      
    description      = "HTTPS"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]  
    ipv6_cidr_blocks = []
    prefix_list_ids = []
    security_groups = []
    self = false
    }]
}